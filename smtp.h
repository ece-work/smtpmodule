#ifndef SMTP_H
#define SMTP_H
#include <QString>
#include <QTextStream>
#include <QDebug>
#include <QtWidgets/QMessageBox>
#include <QByteArray>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QSslSocket>

class Smtp : public QObject
{
    Q_OBJECT

public:
    Smtp();
    ~Smtp();

    void sendMail(const QString from, const QString to, const QString subject, const QString body, QStringList files);

signals:
    void status( const QString &);

private slots:
    void stateChanged(QAbstractSocket::SocketState socketState);
    void errorReceived(QAbstractSocket::SocketError socketError);
    void disconnected();
    void connected();
    void slotReadyRead();

private:
    int timeout;
    QString message;
    QTextStream *txtStream;
    QSslSocket *socket;
    QString from;
    QString rcpt;
    QString response;
    QString username;
    QString password;
    QString host;
    int port;
    enum states{Tls, HandShake, Auth, User, Pass, Rcpt, Mail, Data, Init, Body, Quit, Close};
    int state;
};

#endif // SMTP_H
