#include "smtp.h"

#include <QFile>
#include <QFileInfo>

Smtp::Smtp()
{
    socket = new QSslSocket(this);

    connect(socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorReceived(QAbstractSocket::SocketError)));
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState)));

    qDebug() << QSslSocket::sslLibraryBuildVersionString();
    qDebug() << QSslSocket::supportsSsl();
    qDebug() << QSslSocket::sslLibraryVersionString();

    this->username = "noreply@valeo.com";
    this->password = "";
    this->host = "10.208.8.36";
    this->port = 25;
    this->timeout = 30000;
}

Smtp::~Smtp()
{
    delete txtStream;
    delete socket;
}

void Smtp::connected()
{
    qDebug() << "Connected ";
}

void Smtp::disconnected()
{
    qDebug() <<"disconneted";
    qDebug() << "error "  << socket->errorString();
}

void Smtp::errorReceived(QAbstractSocket::SocketError socketError)
{
    qDebug() << "error " <<socketError;
}

void Smtp::stateChanged(QAbstractSocket::SocketState socketState)
{
    qDebug() <<"stateChanged " << socketState;
}

void Smtp::sendMail(const QString from, const QString to, const QString subject, const QString body, QStringList files)
{
    message = "To: " + to + "\n";
    message.append("From: " + from + "\n");
    message.append("Subject: " + subject + "\n");

    //Let's intitiate multipart MIME with cutting boundary "frontier"
        message.append("MIME-Version: 1.0\n");
        message.append("Content-Type: multipart/mixed; boundary=frontier\n\n");

        message.append( "--frontier\n" );
        //message.append( "Content-Type: text/html\n\n" );  //Uncomment this for HTML formating, coment the line below
        message.append( "Content-Type: text/plain\n\n" );
        message.append(body);
        message.append("\n\n");

        if(!files.isEmpty())
        {
            qDebug() << "Files to be sent: " << files.size();
            foreach(QString filePath, files)
            {
                QFile file(filePath);
                if(file.exists())
                {
                    if (!file.open(QIODevice::ReadOnly))
                    {
                        qDebug("Couldn't open the file");
                        QMessageBox::warning(0, tr("Qt Simple SMTP client"), tr("Couldn't open the file\n\n"));
                            return ;
                    }
                    QByteArray bytes = file.readAll();
                    message.append("--frontier\n");
                    message.append("Content-Type: application/octet-stream\nContent-Disposition: attachment; filename="+ QFileInfo(file.fileName()).fileName() +";\nContent-Transfer-Encoding: base64\n\n" );
                    message.append(bytes.toBase64());
                    message.append("\n");
                }
            }
        }
        else
            qDebug() << "No attachments found";


        message.append( "--frontier--\n" );

    message.append(body);
    message.replace(QString::fromLatin1("\n"), QString::fromLatin1("\r\n"));
    message.replace(QString::fromLatin1("\r\n.\r\n"), QString::fromLatin1("\r\n..\r\n"));
    this->from = from;
    rcpt = to;
    state = Init;
    socket->connectToHostEncrypted(host, port);
    if (!socket->waitForConnected(timeout)) {
         qDebug() << socket->errorString();
     }

    txtStream = new QTextStream(socket);
}

void Smtp::slotReadyRead()
{
     qDebug() <<"readyRead";

    QString responseLine;
    do
    {
        responseLine = socket->readLine();
        response += responseLine;
    }

    while (socket->canReadLine() && responseLine[3] != ' ');

    responseLine.truncate(3);

    qDebug() << "Server response code:" <<  responseLine;
    qDebug() << "Server response: " << response;

    if (state == Init && responseLine == "220")
    {
        // response okay, let's go on
        *txtStream << "EHLO localhost" <<"\r\n";
        txtStream->flush();
        state = HandShake;
    }

    else if (state == HandShake && responseLine == "250")
    {
        socket->startClientEncryption();
        if(!socket->waitForEncrypted(timeout))
        {
            qDebug() << socket->errorString();
            state = Close;
        }

        *txtStream << "EHLO localhost" << "\r\n";
        txtStream->flush();
        state = Auth;
    }

    else if (state == Auth && responseLine == "250")
    {
        // Trying AUTH
        qDebug() << "Auth";
        *txtStream << "AUTH LOGIN" << "\r\n";
        txtStream->flush();
        state = User;
    }

    else if (state == User && responseLine == "334")
    {
        //Trying User
        qDebug() << "Username";
        *txtStream << QByteArray().append(username).toBase64()  << "\r\n";
        txtStream->flush();
        state = Pass;
    }

    else if (state == Pass && responseLine == "334")
    {
        //Trying pass
        qDebug() << "Password";
        *txtStream << QByteArray().append(password).toBase64() << "\r\n";
        txtStream->flush();
        state = Mail;
    }

    else if (state == Mail && responseLine == "235")
    {
        // HELO response was okay
        qDebug() << "MAIL FROM:<" << from << ">";
        *txtStream << "MAIL FROM:<" << from << ">\r\n";
        txtStream->flush();
        state = Rcpt;
    }

    else if (state == Rcpt && responseLine == "250")
    {
        qDebug() << "RCPT TO:<"  << rcpt << ">";
        *txtStream << "RCPT TO:<" << rcpt << ">\r\n";
        txtStream->flush();
        state = Data;
    }

    else if (state == Data && responseLine == "250")
    {
        qDebug() << "DATA";
        *txtStream << "DATA\r\n";
        txtStream->flush();
        state = Body;
    }

    else if (state == Body && responseLine == "354")
    {
        qDebug() << "Message";
        *txtStream << message << "\r\n.\r\n";
        txtStream->flush();
        state = Quit;
    }

    else if (state == Quit && responseLine == "250")
    {
        *txtStream << "QUIT\r\n";
        txtStream->flush();
        // here, we just close.
        state = Close;
        //emit status(tr("Message sent"));
        //QMessageBox::information(0, tr("Qt Simple SMTP client"), tr("Message sent!\n\n"));
        qDebug() << "Message sent!";
    }

    else if (state == Close)
    {
        deleteLater();
        return;
    }

    else
    {
        // something broke.
        //QMessageBox::warning(0, tr("Qt Simple SMTP client"), tr("Unexpected reply from SMTP server:\n\n") + response);
        state = Close;
        //emit status(tr("Failed to send message"));
        qDebug() << "Failed to send message";

    }
    response = "";
}
